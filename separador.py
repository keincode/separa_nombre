from pathlib import Path
from tkinter import ttk
import tkinter as tk
from tkinterdnd2 import TkinterDnD, DND_FILES


class Interfaz(TkinterDnD.Tk):
    PARTES = []
    def __init__(self, **kwargs):
        super(Interfaz, self).__init__(**kwargs)
        self.geometry('420x140')
        
        self.partes = tk.StringVar(value=self.PARTES)
        self.lw = tk.Listbox(self, listvariable=self.partes, selectmode='multiple')
        self.lw.grid(row=0, column=0, sticky='ns')
        fm = tk.Frame(self)
        fm.grid(row=0, column=1, sticky='wens')
        self.info = tk.Text(
            fm, bg='black', fg='white', padx=10, pady=5
        )
        self.info.grid(row=1, column=0, sticky='wens')
        self.fmb = tk.Frame(fm)
        self.fmb.grid(row=0, column=0, sticky='we')
        self.bt_clear = tk.Button(self.fmb, text='LIMPIAR')
        self.bt_clear.grid(row=0, column=0)
        # self.MODO = tk.StringVar(value=mo)
        valores = ["", "upper", 'lower', 'capitalize', 'camel']
        self.cmb = ttk.Combobox(self.fmb, width=80, values=valores)
        self.cmb.current(1)
        self.cmb.grid(row=0, column=1)
        self.bt_copy = tk.Button(self.fmb, text='COPIAR')
        self.bt_copy.grid(row=0, column=3)
        self.sci = ttk.Scrollbar(fm, orient='vertical', command=self.info.yview)
        self.sci.grid(row=1, column=0, in_=fm, sticky='ens')
        self.info.config(yscrollcommand=self.sci.set)
        self.bt_top = tk.Button(self.fmb, text='TOP')
        self.bt_top.grid(row=0, column=2)

        self.RESULTADO = tk.StringVar()
        self.en = tk.Entry(self, textvariable=self.RESULTADO)
        self.en.grid(row=1, column=0, columnspan=2, sticky='we')

        self.bt_texto = tk.Button(self.fmb, text='TEXTO')
        self.bt_texto.grid(row=0, column=4)

        self.rowconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        fm.columnconfigure(0, weight=1)
        fm.rowconfigure(1, weight=1)
        self.fmb.columnconfigure(1, weight=1)
        self.title('SEPARADOR')

    def info_escribe(self, texto):
        self.info.insert('end', texto)
        self.info.see('end')

    def info_borrar(self):
        self.info.delete('0.1', 'end')

    def a_clipboard(self, texto):
        self.clipboard_clear()
        self.clipboard_append(texto)
        self.update()

    def modo(self):
        return self.cmb.get()

    def _fix_rutas(self, texto):
        archivos = []
        res = []
        _ = ':'
        inicial = 0
        for x in range(texto.count(_)):
            indice = texto.index(_, inicial, -1)
            res.append(indice-1)
            inicial = indice+1
        res.append(-1)

        inicial = 0
        for indice in res[1:]:
            url = texto[inicial:] if indice==-1 else texto[inicial:indice]
            archivos.append(url.strip('{} '))
            inicial = indice
        return archivos
    
    def msg(self, texto, tag='_tg', fg=None, bg=None, **kwargs):
        self.info.insert('end', texto, (texto, tag))
        if fg is not None:
            self.info.tag_config(tag, foreground=fg, **kwargs)
        if bg is not None:
            self.info.tag_config(tag, background=bg, **kwargs)

class Separador(Interfaz):
    STEM = None
    ON_TOP = False
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.drop_target_register(DND_FILES)
        self.dnd_bind('<<Drop>>', self.obten_drop)
        self.lw.bind('<<ListboxSelect>>', self.elegidos)
        self.bt_clear.config(command=self.deselecciona_todo)
        self.bt_copy.config(command=self.elegidos)
        self.bt_top.config(command=self.mantener)
        self.bt_texto.config(command=self.obten_texto)
        self.colorea()
        self.attributes('-topmost', True)

    def obten_drop(self, e):
        archivos = self._fix_rutas(e.data)
        ruta = Path(archivos[0])
        folder = ruta.parent.as_posix()
        # self.info_escribe(folder)
        ye1 = '#F4CA16'
        self.msg(f"{folder}\n", fg=ye1,tag='fol')
        self.STEM = ruta.stem
        self.msg(f"{self.STEM}\n", tag='file')
        self.separa_nombre()
        self.info.see('end')

    def separa_nombre(self):
        self.PARTES = self.STEM.strip().split()
        self.partes.set(self.PARTES)

    def elegidos(self, e=None):
        indices = self.lw.curselection()
        palabras = [self.lw.get(i) for i in indices]
        salida = ' '.join(palabras).strip().replace("'", "")
        modo = self.modo()
        if modo:
            if modo=='camel':
                salida = ''.join([pal.capitalize() for pal in palabras])
            else:
                salida = eval(f"'{salida}'.{modo}()")
        self.a_clipboard(salida)

        self.en.delete('0', 'end')
        self.RESULTADO.set(salida)

    def deselecciona_todo(self):
        self.lw.delete(0, 'end')
        if self.PARTES:
            self.separa_nombre()
        self.en.delete(0, 'end')
        self.info_borrar()

    def colorea(self):
        bg1 = '#0A042F'
        fg1 = '#D6DCF0'
        ye1 = '#F4CA16'
        bk1 = '#000000'
        fo1 = ('Arial', 8, 'normal')
        fo2 = ('Consolas', 8, 'bold')
        bg2 = '#0A042F'
        self.lw.config(
            background=bg1, fg=fg1, takefocus=False,
            selectbackground=ye1, selectforeground=bk1,
            highlightthickness=0, borderwidth=0,
            activestyle='none', justify='left',
            relief='flat'
        )
        self.info.config(
            background=bg1, fg=fg1,
            highlightbackground=ye1,
            highlightcolor=bk1,
            relief='flat',
            pady=5, font=fo1,
            padx=15,
            selectbackground=ye1,
            selectforeground=bk1,
            insertbackground=ye1
        )
        self.en.config(
            background=bg1, fg=ye1, font=fo2,
            insertbackground=ye1,
            selectbackground=ye1,
            selectforeground=bk1,
            relief='flat',
            borderwidth=0,
        )
        self.fmb.config(background=bg1)
        sbtn = {
            'background':bg2, 'fg':ye1,
            'relief':'flat', 'borderwidth':0,
            'activebackground':ye1,
            'takefocus':False,
            'font':fo2
        }
        self.bt_copy.config(**sbtn)
        self.bt_clear.config(**sbtn)
        self.bt_top.config(**sbtn)
        self.bt_texto.config(**sbtn)
        bgc = bg1
        s = ttk.Style()
        s.theme_use('alt')
        s.configure(
            'TCombobox',
            foreground=ye1,
            selectbackground=bg1,
            selectforeground=ye1,
            fieldbackground=bg1,
            background=bg1,
            bordercolor=bgc,
            # focusfill='green',
            lightcolor='black',
            darkcolor='black',
            arrowcolor=ye1,
            relief='flat',
            # border=0,
            # borderwidth=0,
            # highlightthickness=0
        )
        s.map('TCombobox', background=[('!active', 'readonly', bg1)])
        self.option_add('*TCombobox*Listbox.background', bg1)
        self.option_add('*TCombobox*Listbox.foreground', ye1)
        self.option_add('*TCombobox*Listbox.selectBackground', ye1)
        self.option_add('*TCombobox*Listbox.selectForeground', bg1)

        s.configure("Vertical.TScrollbar", gripcount=0,
                background=ye1, darkcolor="black", lightcolor="black",
                troughcolor=bg1,
                bordercolor="black",
                relief='flat'
                )
        s.map("TScrollbar",
          background=[('!active', bg1)],
          bordercolor=[('active', bg1)],
          lightcolor=[('active', bg1)],
          darkcolor=[('active', bg1)],
          arrowcolor=[('active', 'black'), ('!active', ye1)],
          )
        

        self.ico = {
            'clear':'''iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC7ElEQVR4nHWSXYiUZRTHf
            +d53plxdlx3a5Gd2cpgP6zGmXnn3UWJoC50q73oQwvJi7IPiCiipQjFveimG6msTVqITIxFCoJQsbQlC
            Qs2kSTZTaUP+mS1qDak3Xbn432e08VMaaB/+F+d8zsHzvkLl5YAGIN6H24AuxbkbEd6fvzPytfnVLGAu
            wzbgK0FKG9fc11Bd23t0SfvuUFTUjoNpYIIAPZysAksQDg62F/U3ya6nE53xvplZ233tl5NB+F3icRA8
            VJDBKABl3cM9hf0/EddsR7P+sVPclr7NKc61Rm/ua1HU7Y4A6tCkSbUhCWw+NhFo2ujePjdF2ZdxzK19
            SoYAwo4D6l27956p9U+8VLmG7XBraYJawMuv7wuqg8f3vmH6+jwprIIxl7YEBionTf2ofvn6k9trK6sV
            PzjBhBVJHbR6GDknp4Ym3VHjifNiZMJWdKmuPj/R3Ie8JibilUH2m2M4EXK29f1x8MHx353H0wmzfot7
            bJppJ3pMwmSrYprPswrLEmBnzP+jQMZC+YIUL579cqCLhztig++eK1PB6GmbaiWsvZ1FfXU21ernsxq5
            WhO48+yWjuWq2++La9Q3js01JsCotf2Ptfjft53VT0poaZsqK2phg1l7csV9djuFepPZLU6mXObb88rh
            Pvzy/NLATGgM5+fTphrso67bq5RdYIRMALppPLtL5bvfzKIxT/2fJsZn0juSSWn1n81e2YeUJa3rM4mb
            enUnpFe1S+y9XtvuV4harqsO5/p1oWPc/6OG/MK0QwMPAgDGyG6L0OhU0RANVxl4dCukb9XPLxhLt76y
            jLzw68BQ2sqPPLAAu/tTzP+YZorMi6ux5IwCTh7LiGT0/Z1AawILmgpFVg0B8aeXeh+dNMceKAu/DUvZ
            NKKbdFGmmLgSs/7+5Zy55a2Q/8m0Qo4pVAKsDtaW7TPO8TaRgq9B6f/RUEF1Dnj49i/KlyQEcEDqA70g
            L+4dnGbgjOgdZj68R8lDTrVqjsaUwAAAABJRU5ErkJggg==''',
            'copiar':'''iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACAklEQVR4nJ2SMUhVURjH
            f9859773NBUHsYjKpUHMQTPMTcoMp5bXbakl14JqjVDuEA1BU60NDg0+cIhaQgjdEkQsoiJqiCJwSND0
            2X33nK/hel88ngb1nw7n/P//7/t/5xMaEFmRilMdDaCtnT3xfF0EVBFApS4FO2dwzg9EY4O1qZ7D6UGX
            IhkNEEDQpdfh8rtvxesiy59UMbnBLu1U+cr4z9mZe5tGQtdcPIAvH1qYuNm6cOnqxbMQE8C0gdj3Hum/
            e6gruX1hdMdLKfVba0ZCmxXPKziPHuut6nBfYSCOX7UFlo3A2tg7F7WdGVq9dn6k5td+GNRjAgvWNjag
            gCYYAYWqAfIM22FHq2qaYhREhH0hmbrOMLvhNNW/C/eDqTv/u7bR4H/RZKC6F62JU2cF+UEEvEIhVJyH
            1NWXI3snuyuCBvZP4iAjFNIkISkVVBdXCkx2KAfwTf0VDJ7twH7+atahe7uWIjI9jYlj/Ime/vn5hxtj
            tx60JN1dGvYfT3G1rDMUjKCdnd49WyglMy+KkyKrs6qRFYiskYrzenLk8nj15f0bW6Xl95adnWxJVLMY
            3sKjJ+0svrFlY1bmvI8sVFyexYjgVYdO9x1N7pwb/jVYDH0xH6ix4reqpja/FDz++P3tVLkc2Uql4vLZ
            1H/EGLz3ABMdzYtsU3i6SeNs+Q186MvVCi6NLQAAAABJRU5ErkJggg==''',
            'lock':'''iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACVUlEQVR4nG2TT0hUURTGf+
            feN46auTIJRA2dQM1BwkUFqbRslZBP2hQEgovQKIgIhFlYiPQHtBbRwpKWSrSrIBASBJ1Fi5KQIAOhTR
            BojKPz3n2nxXNycDxw4XLOd75z7nfOhXIzIgACnO+IjxD7MAfBckhypHq69/rFncn+C/lOgLcLVV9fvq
            u8K/L5kyoGiA4pjFFFoDs9PpTK62qDZmeaNTvTrLraoONDqW04m44x5Z2QyfR5vo/tSXfMarZR7w+3bk
            D3JejufzDcuqHZRu1Jd8z6PjaT6fPKCBJ7rluDqUVdaorqa7rGirH6mq4xXWqKbg2mFkuxAF5RhyD0Ew
            nz46qxmy0ucnK8Luyrre0UgOqKsNdFToylJZnoHtoNjr2G9wUA8X3fzs/PuTPtp6ae3cmPtp3YIekpf7
            ctqrHGIsrRaseuE9bWK7kxWTW9vLZ6c2DAt+JZCJ0wcvnkl+mJrfbot6CKtZ5GJWobF4oRwZk6ZfRe7b
            enb76nPav7agoUopzYwMVPCgMxJPFI4oWhGIDAIVFerAiFUg1iU8SYWBAFxIOHL44Awu1rOdTFMRMDpJ
            ygyKNgDBQKMPGqBoCRKzkqEuBc2fDKCUQgcpBMKgvP/wDx3QX/1/lwAmPARWjowDOo2xG62gIAdvNxch
            iBjVBTsodeECIi6FZOxFaLVBunIILszyBZpaBQgSqVVrZysrc7iDcovhGZcx9XKh8/moqmWpqC2qCA2g
            PtOoVEBbq+kdj8sJJ8IqIMim+KsD3xzzVD0ADm0N8GkQH7C5Z/FnP+AXaK5W83DSG2AAAAAElFTkSuQm
            CC''',
            'unlock':'''iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACYElEQVR4nG2TT0hVQRTG
            f2fmPp+aWFhEYGVUgpZmYVBBJS1atErKa20KhEAINAqiguAtKir6A1aLaNEfqJUStYogkAoCddGiJCSw
            wHATFFrPp+/emdPi+kx8DgwMZ77vm8N3voHiZUQABNi9KdlCUsMsBMsiZK+6bW/HgenrrftyDQAv+ss+
            P3pVek7k4ztVDOAXeRijikBz46UTG3M6XK1DD2t06GGN6nC1Zjpqp2BnY4Ip7oRMpiUIQ+yexk1PdGiN
            Xu7cMAbNB6G59UrnhnEdWa37t9c/C8PQZjItQZFAarZ0un3je/2w1q+saLpYuFuW3tL18nrtxKHddS8A
            rP3PCwo+RHGYSpnRY8ZOrHfeyaoVcUtlZYOkrNoo9uVHzi95v7wq/r00vfXExMzSp/B2BkDCMLR9fb1u
            R/3mnntnc91166ZJB8qfKYuqUBiIEQ8GRn+kOXmt7M7AyPCptrbQSmAhdkLX4dpPd65O1vufgirWBurn
            3BYEjwFiqjDdFyq/3H3+tTGwypwZAnmfFRs5fGAgjsQEFWoA4r/JtJ3HpnIYEfLzPUiWIsYkhiggAdx4
            sAQQzhzPog5EwCQAKRYo6CgYA/k8XH1cAUDX0SwlKXCuaHjFAiLgHaTTSv/9X0BydtFcnBcXMAacR2MH
            gUHdtNBUFwEwk0vIsQfrUTMvh0EUIyLoZFbElouUG6cgyfxmE58uU1AoQZVSK5NZmc0OErRLaER63ZvB
            0ls3e3zP+rVRZZRH7YJ2nUKqBP02lpp4PZi+LaK0S2gKsFnzd9VAVA1m0d8G3oAdh4HvBc4/cTLkNtQ6
            4FQAAAAASUVORK5CYII=''',
            'x':'''iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAACgElEQVR4nFWQTUhUYRiFz/ddR
            8UZM8u89zLT+FM0zu/97rhKCxMqaGeh0A8ZbVzbonJRC6EWLVpVCyWiCMIooqiIoh/QmQkzdWpTWEJEG
            wkqCCK9936nxVjQ2Z73gfM+ACAaarLtgFuQUB6QXwjXOL2oRKyLpJOAO2dAeYA7Fw5lcgAAEkIK5+G+7
            VkWrse8kwcSBNyv8aYO25AAoN4c3pVm4UZs5eieNKVQ0wOpVDW6Eol6AbU0fy0e8L2p+db09m7LEFATh
            nDO5toUl4uWx4VmvXg7FoSE87OprsMGCQmoqZGDHWTZ9L2izc/3onrDGoeAy+JYPOCMRc6Z/nB/koAqk
            DAAAJFap7tKuMGj8y0+Zy3NssmLx9r1cP8WzbJFzlv65mhbAOR/N9RlO1f/HzCEAKR0TkTXufzyIOpzx
            qI3ZXNl0ibnLL6biAWN4TxrQs6QEBVmFe6pqohwb+3IZblcsn1vyqZfsvn9me1nWx0CziUpK7cAIFdJ+
            AEkgHBjvYY0CBKgBqQBhGs1AFEn8C/i31QBZ3ST5fLbU9vX0xZZsqmLNjlj8dPdqG825FklneH/pkZqV
            U9I5vWLCy0+X1uaZYunBjfryyNtFTmzlr5/rtWXcL362sxWAMDYUGcIUDOnBxPkvOnzlc3SeDwAXK6PO
            Fy8E9XBS5ucN/3j+zsIqEkSEk11nXZIOD8Xb8cCfmzWvybtlXRc0RDqDKCu9Do5ctZc4YdmXb4a9wXUU
            k8qFcHQUGdIClU4tDPDJ+Mb/b7uDAF32pBAVyJRD7gLR3an+Hh8Y9DXnaUU6gGJiqdIdToJ5IuA+wNwn
            1trky2r5tAYTqaBfOlv11CTawMg/gBjNhWCGVGCJwAAAABJRU5ErkJggg==''',
            'texto':'''iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACAUlEQVR4nI2TwUtUURjFf
            9+9774ZZx6RlkT7QpjQtAlaRWG5M0TEhYsokFzXIrB2bQqifyDBoFWLiBbVJkQhaddgGVjmxl2QZgnjj
            PPe3HtbjONIzohnfX9wzvnOFWpSgEM4nHyDEUAAH9J7OkZ6DkFLGLrFOF78AYgApPTZywr1NmojqxR43
            xwVAeegWGbL4a5V7Je5QCmoWCau9FWzM9NrWyQY/I6v/20LYEgGxjuzMwvqllLMBSMj6Nev+Ph9NRi7P
            tme1UrqGZtYAOt8+G01IFB2fngELQBPJzB3nvVMlqr6Yi2Ab1GneBAygZu/O5w8fvByKZZI9w4VrTwEy
            YBrZn5fi6AEKEc6uSdKev+MD1aOjg2WSEqCVgfT1oHJeF68yTD9LtxUDbutgrcwsRNJIt09VLTmEXAS/
            Bb45GBYTC0uPyOd3BeA0dFcyC/C2YK6+rtobgCZnZHtkfJA6ViUPO/Pu5n2rrbK1FQhEcgbKCSpVN8pb
            d1C/3kbHe+wOCu7bXpAac/6hmb2ky5WlZyL488rkDcBRB7AV123MUH05Pb6dldXVbEttbUDOCDtWV4O3
            IWbJ6JqYs8AKxD5+l/gCLn2WJn36ZTOh8bv71QgToRyxRbSzg1s8vUvNG4ugO8kF21gLllUull9Gl/uI
            P6wxlKxzuwtSnbv02pKfv/bfyT8wQQ1IbtGAAAAAElFTkSuQmCC''',
            'sep':'''iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAArklEQVR4nKWS0Q3DIAxEfRE
            DIDEAY4QZ2jmYir9OE8ZggEhs4P6QCjmGJu1JSGBs8+AAM280EIDAzC8icm0c2gE8iYgMEZH1aZXFtcT
            cps769JD7zLwBCEYka3Ja0Pq0MvN2heAU6/OvEEx1i0DL+5mglpgBBNOsUq1s8X1E9XnEEZ5W1OfVEvN
            yF13KyIBm1Yzo1OCbAIRpg9HJIy1H19nPmxL1C2mnZrG8wt96A6ldXlU9SbizAAAAAElFTkSuQmCC'''
        }
        self.ico_clear = tk.PhotoImage(data=self.ico.get('clear'))
        self.ico_copiar = tk.PhotoImage(data=self.ico.get('copiar'))
        self.ico_top = tk.PhotoImage(data=self.ico.get('lock'))
        self.ico_texto = tk.PhotoImage(data=self.ico.get('texto'))
        self.ico_pro = tk.PhotoImage(data=self.ico.get('sep'))
        self.bt_clear.config(image=self.ico_clear, compound='left', text='BORRAR', width=60)
        self.bt_copy.config(image=self.ico_copiar, compound='left', width=60)
        self.bt_top.config(image=self.ico_top, compound='left', width=35)
        self.bt_texto.config(image=self.ico_texto, compound='left', width=50)
        self.iconphoto(True, self.ico_pro)

    def mantener(self):
        self.ico_on = tk.PhotoImage(data=self.ico.get('lock'))
        self.ico_off = tk.PhotoImage(data=self.ico.get('unlock'))
        icono = self.ico_on if self.ON_TOP else self.ico_off
        self.bt_top.config(image=icono)
        self.attributes('-topmost', self.ON_TOP)
        self.update()
        self.ON_TOP = not self.ON_TOP

    def obten_texto(self):
        self.STEM = self.info.get('0.1', 'end')
        self.separa_nombre()


if __name__=="__main__":
    app = Separador()
    app.mainloop()