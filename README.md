# Separador de palabras
Hecho con python 3.10 y tkinter

Separa una frase que escribas o nombre de archivo que arrastres sobre el programa lo muestra en una lista y al selecionarla esta se copia al portapapeles, segun la opcion que este seleccionada convertira el texto en mayusculas, minusculas, camelcase, capitalize

**librerias:**
* tkinterdnd2-universal==1.7.3


![](para_md/sep_cap.gif)

en la parte de abajo muestra una previa de lo que esta copiando al portapapeles

